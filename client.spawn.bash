#!/usr/bin/env bash

################################################################################
##
## Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
##
################################################################################

clear

curl --insecure --verbose \
     --header "Content-Type: application/json" \
     --data '{"city":"Copenhagen,Denmark"}' \
     https://localhost.uniprocess.org:8443/
