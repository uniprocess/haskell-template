#!/usr/bin/env bash

################################################################################
##
## Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
##
################################################################################

clear

uid=$1
con="./tmp/con.tar"

docker export $uid > $con

echo "Docker Container Reproducible Build Hash:"

# exclude (running) docker container mount file
pat="etc/mtab"
# find all the layer.tar files
for cf in $(tar --exclude="$pat" -tf $con); do
    n=${cf##*/}
    if [ "" != "$n" ]; then
	tar -xf $con --to-stdout $cf |
	    sha256sum | cut -d " " -f 1
    fi
# Hash the content of each file, sort them and combine to a single hash
done | sort | sha256sum | cut -d " " -f 1

find ./tmp -name "con.tar" -delete

echo
