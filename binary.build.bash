#!/usr/bin/env bash

################################################################################
##
## Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
##
################################################################################

clear

uni="uniprocess"

bin="$(stack path --local-install-root)/bin"

echo "### Clearing package.yaml files:"
find ./pkg -name "package.yaml" -delete -print
echo

vers=package.vers.yaml
flag=package.flag.yaml
path=./pkg
pkgs=$(ls $path)
echo "### Generating package.yaml files:"
for d in $pkgs; do
    core=$path/$d/package.core.yaml
    deps=$path/$d/package.deps.yaml
    name=$path/$d/package.name.yaml
    xtra=$path/$d/package.xtra.yaml
    yaml=$path/$d/package.yaml
    echo $yaml
    cat > $yaml <<EOF
$(cat $vers)

$(cat $name)

$(cat $flag)

$(cat $core)

$(cat $deps)

$(cat $xtra)
EOF
done;
echo

echo "### Stack building:" 
#stack build --verbosity debug
stack build
echo

echo "### Clearing (local) binary files:"
find bin -name $uni -delete -print
echo

echo "### Copying binary to local ./bin:" 
cp -v $bin/$uni ./bin
echo

echo "### Clearing all .cabal files:" 
find . -name "*.cabal" -delete -print
echo

echo -e "### Repoducible hash:\n-" $(sha256sum $bin/$uni | cut -d " " -f 1)
echo
