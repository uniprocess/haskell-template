################################################################################
##
## Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
##
################################################################################

FROM haskell:8.2

# Install dependecies needed to compile Haskell libraries
RUN apt-get update && apt-get install --yes \
    xz-utils \
    make

RUN stack --resolver lts-11.11 install base \
    # A time & space-efficient byte arrays (Word8)
    bytestring \
    # Date and time stamps
    time \
    # Linux's kernel random number generator
    random \
    # Berkeley sockets
    network \
    # Native implementation of TLS protocol for Server & Client
    tls \
    # (tls) Default values for TLS parameters
    data-default-class \
    # (tls) Specify hash for validation (SHA256)
    x509 \
    # (tls) Accessing and storing X.509 certificates
    x509-store \
    # (tls) Disable validation for non x509 v3 certificates
    x509-validation \
    # Validate input. Example: A name shouldn't be "42"
    parser-combinators \
    # serializer from and to JSON
    array \
    # serializer from and to JSON
    containers \
    # serializer from and to JSON
    mtl \
    # serializer from and to JSON
    syb

## References
# - Futtetennismo:
#   * Base: https://futtetennismo.me/posts/docker/
#   * File: 2017-11-24-docker-haskell-executables.html
