################################################################################
##
## Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
##
################################################################################

FROM haskell-base-builder as builder

WORKDIR "../"

# copy the contents of the current directory in the working directory
COPY . .

RUN stack --resolver lts-11.11 install && \
    strip /root/.local/bin/uniprocess

FROM fpco/haskell-scratch:integer-gmp

COPY --from=builder /root/.local/bin/uniprocess /bin/
COPY               ./der/dev/console            /dev/console
COPY               ./der/env/.dockerenv         /.dockerenv
COPY               ./der/etc/hostname           /etc/hostname
COPY               ./der/etc/hosts              /etc/hosts
COPY               ./der/etc/resolv.conf        /etc/resolv.conf
COPY               ./tls/cacert.pem             /tls/cacert.pem
COPY               ./tls/uniprocess.public.crt  /tls/uniprocess.public.crt
COPY               ./tls/uniprocess.secret.key  /tls/uniprocess.secret.key

# We provide the possibility to specify the port and an argument
# Examle: docker run -d -p 8443:8443 uniprocess 8443
# https://stackoverflow.com/a/40312311

# Cos we need root in order to server 80 and 443 in Linux, we use Tomcats 8080
# (HTTP) and 8443 (HTTPS) approach
#
# https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers

ENTRYPOINT [ "/bin/uniprocess" ]
CMD        [       "8443"      ] # Default value but can overriden at runtime

## References
# - Futtetennismo:
#   * Base: https://futtetennismo.me/posts/docker/
#   * File: 2017-11-24-docker-haskell-executables.html
