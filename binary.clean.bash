#!/usr/bin/env bash

################################################################################
##
## Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
##
################################################################################

clear

stack clean

find ./bin -name "uniprocess"  -delete -print
find ./bin -name "validation"  -delete -print
find .     -name "*.cabal"     -delete -print
find .     -name ".stack-work" -type d -exec rm -rv "{}" \;
