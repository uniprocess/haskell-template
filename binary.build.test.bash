#!/usr/bin/env bash

################################################################################
##
## Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
##
################################################################################

clear

val="validation"

bin="$(stack path --local-install-root)/bin"
echo "### Clearing binary files:"
find bin -name $val -delete -print
echo

echo "### Ensure package.yaml files are created correctly"
./binary.build.bash && clear

echo "### Copying test binary to local ./bin:" 
cp -v $bin/$val ./bin
echo

echo "### Execute the test suite"
./bin/$val
echo

echo "### Clearing all .cabal files:" 
find . -name "*.cabal" -delete -print
echo
