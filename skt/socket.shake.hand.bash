#!/usr/bin/env bash

clear

# The WebSocket Protocol
#
# 4. Opening Handshake
#
# 4.1. Client Requirements
# https://tools.ietf.org/html/rfc6455#section-4.1
#
# 1. The handshake MUST be a valid HTTP request as specified by [RFC2616].
#
# 2. The method of the request MUST be GET, and the HTTP version MUST be at
# least 1.1.
#
# 4. The request MUST contain a |Host| header field whose value contains /host/
# plus optionally ":" followed by /port/ (when not using the default port).
#
# 5. The request MUST contain an |Upgrade| header field whose value MUST include
# the "websocket" keyword.
#
# 6. The request MUST contain a |Connection| header field whose value MUST
# include the "Upgrade" token.
#
# 7. The request MUST include a header field with the name |Sec-WebSocket-Key|.
# The value of this header field MUST be a nonce consisting of a randomly
# selected 16-byte value that has been base64-encoded (see Section 4 of
# [RFC4648]). The nonce MUST be selected randomly for each connection.
#
# 8. The request MUST include a header field with the name |Origin| [RFC6454] if
# the request is coming from a browser client. If the connection is from a
# non-browser client, the request MAY include this header field if the semantics
# of that client match the use-case described here for browser clients. The
# value of this header field is the ASCII serialization of origin of the context
# in which the code establishing the connection is running. See [RFC6454] for
# the details of how this header field value is constructed.
#
# 9. The request MUST include a header field with the name
# |Sec-WebSocket-Version|.  The value of this header field MUST be 13.

curl --include --no-buffer \
     --request GET "https://echo.websocket.org" \
     --http1.1 \
     --header "Host: echo.websocket.org:443" \
     --header "Upgrade: websocket" \
     --header "Connection: Upgrade" \
     --header "Sec-WebSocket-Key: dGhlIHNhbXBsZSBub25jZQ==" \
     --header "Origin: https://www.websocket.org" \
     --header "Sec-WebSocket-Version: 13" \
     --header "Sec-WebSocket-Protocol: json"

# Note: cURL doesn't support ws:// or wss:// therefore we use https://
