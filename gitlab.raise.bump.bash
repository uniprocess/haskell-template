#!/usr/bin/env bash

################################################################################
##
## Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
##
################################################################################

clear

message="$1"

# we only raise the semver patch number if any changes
status=$(git status) 
if [[ "$status" == *"nothing to commit, working tree clean" ]]; then
    echo "No changes, therefore not raising the version number."
else
    # show changes before updating the package.vers.yaml file
    git status
    
    # update package.vers.yaml
    inputvs=$(cat package.vers.yaml)
    version=${inputvs%.*} 
    patchnr=${inputvs##*.}
    patchpp=$version.$((patchnr + 1))
    echo $patchpp > package.vers.yaml 
 
    # add changes to git, push and show status
    git add .
    git commit -m "$patchpp - $message"
    git push
    git status
  
    echo
    
    echo "Raised (semver patch)" $patchpp
fi

# Note: In order to achieve 0.11.42.0 (minor update), you will have to update
# `package.vers.yaml` content to `version: 0.11.41.(-1)`
