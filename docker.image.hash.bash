#!/usr/bin/env bash

################################################################################
##
## Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
##
################################################################################

clear

img="./tmp/img.tar"

docker save uniprocess > $img

echo "Docker Image Reproducible Build Hash:"

# exclude all opaque files
pat="*/*.wh..wh..opq"
# find all the layer.tar files
for f in $(tar -tf $img --wildcards "*/layer.tar"); do
    # for each of the layer files, SHA256 each the files
    for cf in $(tar -xf $img --to-stdout $f | tar --exclude="$pat" -tf -); do
	n=${cf##*/}
	if [ "" != "$n" ]; then
	    tar -xf $img --to-stdout $f | tar -xf - --to-stdout $cf |
		sha256sum | cut -d " " -f 1
	fi
    done
# Hash the content of each file, sort them and combine to a single hash
done | sort | sha256sum | cut -d " " -f 1

find ./tmp -name "img.tar" -delete

echo
