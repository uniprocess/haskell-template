#!/usr/bin/env bash

################################################################################
##
## Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
##
################################################################################

clear

cas="ca."
uni="uniprocess."
moz="cacert.pem"

# Clean before
echo "### Clearing certificate files:"
find . -name  $moz   -delete -print
find . -name "$cas*" -delete -print
find . -name "$uni*" -delete -print
echo
