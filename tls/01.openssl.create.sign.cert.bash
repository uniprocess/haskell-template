#!/usr/bin/env bash

################################################################################
##
## Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
##
################################################################################

clear

srl="ca.secret.srl"         # Certificate Authority Serial Number
cak="ca.secret.key"         # Certificate Authority Key
cac="ca.public.crt"         # Certificate Authority

csr="uniprocess.secret.csr" # Certificate Signing Request
key="uniprocess.secret.key" # Signed (by CA) Certificate Key
crt="uniprocess.public.crt" # Signed (by CA) Certificate

# Clean before
echo "### Clearing certificate files:"
find . -name $csr -delete -print
find . -name $key -delete -print
find . -name $crt -delete -print
echo

# Create a certificate (for each uniprocess)

## Create the certificate key

echo "### Create the certificate key:"
openssl \
    genrsa \
    -out $key \
    2048
echo - $key
echo

## Create the signing request

echo "### Create the signing request:"
openssl \
    req \
    -new \
    -key $key \
    -out $csr \
    -config example.conf
echo - $csr
echo

## Generate the certificate using the mydomain csr and key along with the CA
## Root key

echo "### Generate the certificate:"
openssl \
    x509 \
    -req \
    -in $csr \
    -CA $cac \
    -CAkey $cak \
    -CAserial $srl \
    -out $crt \
    -days 730 \
    -sha256
echo - $crt
echo

## References
# - https://gist.github.com/fntlnz/cf14feb5a46b2eda428e000157447309
