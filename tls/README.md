Trust and certificates
======================

## Default

The `Uniprocess.Toolbox.Web.Client` is loaded with Mozillas `cacert.pem` which
is retrieved from `curl` website.

This means that you will `trust` most of the `Root-CA` as a normal browser would
do.

If you choose to use `self-signed certificates` aka `poor man's certificates`
for your `uniprocess`, you will not be able to communicate between them unless
you also add either the Certificate Authority (`ca.public.crt`) or the a Signed
(by CA) Certificate (`uniprocess.public.crt`) like this:

```
cat cacert.pem ca.public.crt > cacert.pem
```
or

```
cat cacert.pem ~/unips/foo/tls/uniprocess.public.crt > cacert.pem
```

The first approach would be ideal if you are using a [Docker Compose][compose]
setup, while the second could be if you just need to give access to another
(trusted) `uniprocess` or `service`.

> Remark: If you just want your `uniprocess` to communicate between each other,
> and some trusted `service` to persist the final result, you could drop to
> usage of Mozillas `cacert.pem` and only use your own trusted Certificate
> Authorities and/or Signed (by your own trusted CA) Certificates for the
> `Uniprocess.Toolbox.Web.Client` like this:

```
cat ca.public.crt > cacert.pem
```
or

```
cat ca.public.crt ~/unips/foo/tls/uniprocess.public.crt > cacert.pem
```

[compose]: https://docs.docker.com/compose/install/
