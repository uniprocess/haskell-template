#!/usr/bin/env bash

################################################################################
##
## Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
##
################################################################################

clear

srl="ca.secret.srl" # Certificate Authority Serial Number
key="ca.secret.key" # Certificate Authority Key
crt="ca.public.crt" # Certificate Authority

# Clean before
echo "### Clearing certificate files:"
find . -name $srl -delete -print
find . -name $key -delete -print
find . -name $crt -delete -print
echo

# Create manually CA Serial Number
echo "### Create manually CA Serial Number:"
rnd=$(od -A n -N 5 /dev/urandom | tr -d ' ')
cat > $srl << EOF
$(echo $rnd)
EOF
echo - $srl
echo

# Create Root CA

## Create Root Key

echo "### Create Root Key:"
openssl \
    genrsa \
    -out \
    $key \
    2048
#    -des3 \ If you want a non password protected key just remove the -des3
echo - $key
echo

## Create and self-sign the Root Certificate

echo "### Create and self-sign the Root Certificate:"
openssl \
    req \
    -x509 \
    -new \
    -nodes \
    -key $key \
    -sha256 \
    -days 1024 \
    -out $crt \
    -config example.conf
echo - $crt
echo

## Reference
# - https://gist.github.com/fntlnz/cf14feb5a46b2eda428e000157447309
