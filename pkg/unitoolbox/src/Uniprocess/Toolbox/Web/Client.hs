--------------------------------------------------------------------------------
--
-- Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE OverloadedStrings #-}

--------------------------------------------------------------------------------

module Uniprocess.Toolbox.Web.Client
  ( get
  , get'
  , post
  , post'
  ) where

--------------------------------------------------------------------------------

import           Control.Exception
  ( IOException
  , try
  )
import qualified Data.ByteString            as BS
import qualified Data.ByteString.Char8      as C8
import qualified Data.ByteString.Lazy.Char8 as L8
import           Data.Default.Class
  ( def
  )
import           Data.Maybe
  ( fromMaybe
  )
import qualified Data.X509                  as X509
import qualified Data.X509.CertificateStore as X509
import qualified Data.X509.Validation       as X509
import           Network.Socket             hiding
  ( recv
  )
import qualified Network.TLS                as TLS
import qualified Network.TLS.Extra          as TLSX
import           System.Environment
  ( getExecutablePath
  )

--------------------------------------------------------------------------------

dns
  :: String
  -> String
  -> IO (Either IOException [AddrInfo])

get
  :: String
  -> Maybe Int
  -> Maybe String
  -> IO (Either String L8.ByteString)

get'
  :: String
  -> Maybe Int
  -> Maybe String
  -> Bool
  -> IO (Either String L8.ByteString)

post
  :: String
  -> Maybe Int
  -> Maybe String
  -> String
  -> IO (Either String L8.ByteString)

post'
  :: String
  -> Maybe Int
  -> Maybe String
  -> String
  -> Bool
  -> IO (Either String L8.ByteString)

common
  :: String
  -> Maybe Int
  -> Maybe String
  -> Maybe String
  -> Bool
  -> IO (Either String L8.ByteString)

--------------------------------------------------------------------------------

dns host port =
  try $ getAddrInfo (Just hint) (Just host) (Just port)
  where
    hint = defaultHints { addrSocketType = Stream }

get host port path =
  common host port path Nothing True

get' host port path v3 =
  common host port path Nothing v3

post host port path body =
  common host port path (Just body) True

post' host port path body v3 =
  common host port path (Just body) v3

common host port path body v3 =
  do
    info <- dns host port'
    case info of
      Left  _____ -> pure $ Left $ "No service on " ++ host ++ ":" ++ port'
      Right [   ] -> pure $ Left $ "No service on " ++ host ++ ":" ++ port'
      Right (a:_) ->
        do
          x509 <- cacs
          sock <- socket (addrFamily a) (addrSocketType a) (addrProtocol a)
          ____ <- connect sock (addrAddress a)
          ctx  <- TLS.contextNew sock $ para x509 host
          ___  <- TLS.handshake ctx
          ___  <- TLS.sendData ctx $
            L8.fromChunks $
            [ C8.pack $ met ++ path' ++ " HTTP/1.1"
            , "\r\n"
            , C8.pack $ "Host: " ++ host
            , "\r\n"
            , C8.pack $ "User-Agent: uniprocess/0.11.0.0"
            , "\r\n"
            , C8.pack $ "Connection: close"
            , "\r\n"
            ] ++ bod
          res  <- recv ctx
          return $ Right $ res
          where
            met =
              if body == Nothing
              then "GET "
              else "POST "
            bod =
              case body of
                Nothing ->
                  [ "\r\n"
                  ]
                Just bs ->
                  [ C8.pack $ "Content-Type: application/json; charset=utf-8"
                  , "\r\n"
                  , C8.pack $ "Content-Length: " ++ show len
                  , "\r\n"
                  , "\r\n"
                  , cbs
                  ]
                  where
                    cbs = C8.pack   bs
                    len = C8.length cbs
    where
      port' = show $ fromMaybe 443 port
      path' =        fromMaybe "/" path
      cacs  =
        do
          cert <-
            do
              d <- getExecutablePath >>= \ p -> pure $ take (length p - 10) p
              X509.readCertificateStore $ d ++ "../tls/cacert.pem"
          case cert of
            Nothing -> pure $ X509.makeCertificateStore []
            Just cs -> pure $ cs
      para x509 h =
        ( TLS.defaultParamsClient h BS.empty )
        { TLS.clientHooks           =
          def
          { TLS.onServerCertificate =
            ( X509.validate
              X509.HashSHA256
              X509.defaultHooks $
              X509.defaultChecks { TLS.checkLeafV3 = v3 }
            )
          }
        , TLS.clientShared          =
          def
          { TLS.sharedCAStore       = x509
          }
        , TLS.clientSupported       =
          def
          { TLS.supportedCiphers    = TLSX.ciphersuite_strong
          , TLS.supportedVersions   = [ TLS.TLS12 ]
          }
        }
      recv ctx =
        L8.fromChunks <$> aux
        where
          aux =
            TLS.recvData ctx >>= \ pkg ->
            if   0 == BS.length pkg
            then                  pure $ [       ]
            else aux >>= \ acc -> pure $ pkg : acc
