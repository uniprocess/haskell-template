--------------------------------------------------------------------------------
--
-- Public domain: CC0, creativecommons.org/share-your-work/public-domain/cc0
--
--------------------------------------------------------------------------------

module Uniprocess.Process
  ( process
  ) where

--------------------------------------------------------------------------------

import qualified Data.ByteString.Lazy                 as LBS
import qualified Data.ByteString.Lazy.Char8           as L8

import qualified Uniprocess.Data.JSON                 as JSON
import qualified Uniprocess.Data.Parser.HTTP.Request  as Request
import qualified Uniprocess.Data.Parser.HTTP.Response as Response
import qualified Uniprocess.Data.SHS.SHA.SHA1         as SHA1

import qualified Uniprocess.Domain.API                as API
import qualified Uniprocess.Domain.MET                as MET
import qualified Uniprocess.Domain.OSM                as OSM

import qualified Uniprocess.Effects.Granulated        as Granulated
import qualified Uniprocess.Effects.Restricted        as Restricted

import qualified Uniprocess.Network.HTTP.Request      as HTTP
import qualified Uniprocess.Network.HTTP.Response     as HTTP
import qualified Uniprocess.Network.HTTPS             as HTTPS
import qualified Uniprocess.Network.TLS               as TLS

--------------------------------------------------------------------------------

recvCity
  :: ( TLS.Context a
     , Granulated.WebServerM m
     )
  => HTTPS.Web a
  -> m (Either String API.City)

randUID
  :: ( Granulated.RandM m )
  => m (L8.ByteString)

timeStamp
  :: ( Granulated.DateTimeM m )
  => m String

getCoordinates
  :: ( Granulated.WebClientM m )
  => API.City
  -> m (Either String OSM.Coordinates)

getTemp
  :: ( Granulated.WebClientM m )
  => L8.ByteString
  -> String
  -> OSM.Coordinates
  -> m (Either String API.Temperature)

sendTemp
  :: ( TLS.Context a
     , Granulated.WebServerM m
     )
  => HTTPS.Web a
  -> API.Temperature
  -> m ()

process
  :: ( TLS.Context a
     , Restricted.CmdM       m
     , Restricted.DateTimeM  m
     , Restricted.RandM      m
     , Restricted.WebClientM m
     , Restricted.WebServerM m
     )
  => HTTPS.Web a
  -> m ()

--------------------------------------------------------------------------------

recvCity ws =
  Right . aux <$> Granulated.recv ws
  where
    aux bs =
      (JSON.decodeJSON . L8.unpack) body
      where
        (Right (HTTP.Request _ _ _ (Just body))) = Request.parse bs

randUID =
  LBS.pack . SHA1.bytes <$> Granulated.bytes 512

timeStamp =
  Granulated.iso8601

getCoordinates c =
  aux <$> (Granulated.get host port $ (path . API.city) c)
  where
    aux (Left  er) = Left er
    aux (Right bs) =
      (
        case coords of
          [ ] -> Left  "No lat/lon coordinates for the provided city."
          x:_ -> Right x
      )
      where
        (Right (HTTP.Response _ _ (Just body))) = Response.parse bs
        coords = (JSON.decodeJSON . L8.unpack) body
    host   = "nominatim.openstreetmap.org"
    port   = Nothing -- Just $ 443
    path x = Just $ "/search?q=" ++ x ++ "&format=json"

getTemp r t c =
  aux <$> (Granulated.get host port $ path lat lon)
  where
    lat = OSM.lat  c
    lon = OSM.lon c
    r2e x =
      case x of
        JSON.Ok    o -> Right o
        JSON.Error e -> Left  e
    aux (Left  er) = Left er
    aux (Right bs) =
      (
        r2e $ (JSON.decode . L8.unpack) body
      ) >>=
      Right .
      API.Temperature r t .
      MET.value .
      MET.temperature .
      MET.location .
      MET.time .
      MET.product
      where
        (Right (HTTP.Response _ _ (Just body))) = Response.parse bs
    host     = "api.met.no"
    port     = Nothing -- Just $ 443
    path x y =
      Just $ "/weatherapi/locationforecast/1.9/.json?lat=" ++ x ++ "&lon=" ++ y

sendTemp ws =
  Granulated.send ws . L8.pack . JSON.encodeJSON

process webserver@(HTTPS.Server _) =
  recvCity webserver      >>= \ (Right city) ->
  getCoordinates city     >>= \ (Right cord) ->
  randUID                 >>= \        ruid  ->
  timeStamp               >>= \        time  ->
  getTemp ruid time cord  >>= \ (Right temp) ->
  sendTemp webserver temp

process (HTTPS.Socket _) =
  error "WebSocket still not implemented."
