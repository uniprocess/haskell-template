--------------------------------------------------------------------------------
--
-- Public domain: CC0, creativecommons.org/share-your-work/public-domain/cc0
--
--------------------------------------------------------------------------------

{-# LANGUAGE DeriveDataTypeable #-}

--------------------------------------------------------------------------------

module Uniprocess.Domain.API
  ( City
    ( City
    , city
    )
  , Temperature
    ( Temperature
    , temperature
    , timestamp
    , uniqueidentifier
    )
  ) where

--------------------------------------------------------------------------------

import qualified Data.ByteString.Lazy.Char8 as L8
import           Data.Data

--------------------------------------------------------------------------------

data City = City
  { city :: String
  } deriving (Data, Typeable)

data Temperature = Temperature
  { uniqueidentifier :: L8.ByteString
  , timestamp        :: String
  , temperature      :: Double
  } deriving (Data, Typeable)
