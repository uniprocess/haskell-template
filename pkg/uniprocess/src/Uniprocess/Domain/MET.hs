--------------------------------------------------------------------------------
--
-- Public domain: CC0, creativecommons.org/share-your-work/public-domain/cc0
--
--------------------------------------------------------------------------------

module Uniprocess.Domain.MET
  ( LocationForecast
    ( product
    )
  , Product
    ( time
    )
  , Time
    ( location
    )
  , Location
    ( temperature
    )
  , Temperature
    ( value
    )
  ) where

--------------------------------------------------------------------------------

import           Prelude              hiding
  ( product
  )
import qualified Uniprocess.Data.JSON as JSON

--------------------------------------------------------------------------------

data LocationForecast = LocationForecast
  { product :: Product
  }

data Product = Product
  { time :: Time
  }

data Time = Time
  { location :: Location
  }

data Location = Location
  { temperature :: Temperature
  }

data Temperature = Temperature
  { value :: Double
  }

--------------------------------------------------------------------------------

instance JSON.JSON LocationForecast where
  readJSON =
    aux
    where
      aux (JSON.JSObject x) =
        (
          pro $
          filter ((== "product") . fst) $
          JSON.fromJSObject x
        ) >>= JSON.Ok . LocationForecast
      aux ____________ =
        JSON.Error "Not valid `LocationForecast` JSON object"
      pro [("product", JSON.JSObject x)] =
        (
          tim $
          filter ((== "time") . fst) $
          JSON.fromJSObject x
        ) >>= JSON.Ok . Product
      pro _________________________ =
        JSON.Error "No `product` is present"
      tim [("time", JSON.JSArray xs)] =
        (
          loc $
          filter ((== "location") . fst) $
          (
            if   [] == xs
            then []
            else
              case head xs of
                JSON.JSNull           -> [                 ]
                (JSON.JSBool _)       -> [                 ]
                (JSON.JSRational _ _) -> [                 ]
                (JSON.JSString _)     -> [                 ]
                (JSON.JSArray _)      -> [                 ]
                (JSON.JSObject x)     -> JSON.fromJSObject x
          )
        ) >>= JSON.Ok . Time
      tim ______________________ =
        JSON.Error "No `time` is present"
      loc [("location", JSON.JSObject x)] =
        (
          tem $
          filter ((== "temperature") . fst) $
          JSON.fromJSObject x
        ) >>= JSON.Ok . Location
      loc __________________________ =
        JSON.Error "No `location` is present"
      tem [("temperature", JSON.JSObject x)] =
        (
          val $
          filter ((== "value") . fst) $
          JSON.fromJSObject x
        ) >>= JSON.Ok . Temperature
      tem _____________________________ =
        JSON.Error "No `temperature` is present"
      val [("value", JSON.JSString (JSON.JSONString x))] = JSON.Ok $ read x
      val ____________________________________ =
        JSON.Error "No `value` is present"
  showJSON = undefined -- Not needed

--------------------------------------------------------------------------------

{- Meteorologisk institutt (Norway) API

-- HTTP Request (GET):
https://api.met.no/weatherapi/locationforecast/1.9/.json?lat=55.676098&lon=12.568337

-- HTTP Response:
{
  "created": "2019-02-08T15:23:05Z",
  "meta": {
    "model": [
      {
        "nextrun": "2019-02-08T16:00:00Z",
        "from": "2019-02-08T16:00:00Z",
        "termin": "2019-02-08T12:00:00Z",
        "name": "LOCAL",
        "runended": "2019-02-08T14:42:19Z",
        "to": "2019-02-11T06:00:00Z"
      },
      {
        "to": "2019-02-18T00:00:00Z",
        "runended": "2019-02-08T07:34:07Z",
        "name": "EC.GEO.0125",
        "nextrun": "2019-02-08T19:00:00Z",
        "from": "2019-02-11T12:00:00Z",
        "termin": "2019-02-08T00:00:00Z"
      }
    ]
  },
  "product": {
    "time": [
      {
        "from": "2019-02-08T16:00:00Z",
        "datatype": "forecast",
        "to": "2019-02-08T16:00:00Z",
        "location": {
          "cloudiness": {
            "id": "NN",
            "percent": "100.0"
          },
          "pressure": {
            "unit": "hPa",
            "id": "pr",
            "value": "1005.3"
          },
          "highClouds": {
            "id": "HIGH",
            "percent": "96.4"
          },
          "windSpeed": {
            "mps": "4.8",
            "name": "Lett bris",
            "id": "ff",
            "beaufort": "3"
          },
          "windGust": {
            "id": "ff_gust",
            "mps": "9.0"
          },
          "fog": {
            "percent": "0.0",
            "id": "FOG"
          },
          "lowClouds": {
            "id": "LOW",
            "percent": "99.3"
          },
          "latitude": "55.6761",
          "temperature": {
            "unit": "celsius",
            "id": "TTT",
            "value": "5.3"
          },
          "humidity": {
            "unit": "percent",
            "value": "86.6"
          },
          "windDirection": {
            "id": "dd",
            "deg": "193.1",
            "name": "S"
          },
          "mediumClouds": {
            "percent": "87.3",
            "id": "MEDIUM"
          },
          "longitude": "12.5683",
          "areaMaxWindSpeed": {
            "mps": "9.2"
          },
          "altitude": "12",
          "dewpointTemperature": {
            "id": "TD",
            "unit": "celsius",
            "value": "3.2"
          }
        }
      },
      {
        "to": "2019-02-08T16:00:00Z",
        "location": {
          "latitude": "55.6761",
          "altitude": "12",
          "longitude": "12.5683",
          "precipitation": {
            "minvalue": "0.0",
            "unit": "mm",
            "maxvalue": "0.0",
            "value": "0.0"
          },
          "symbol": {
            "id": "Cloud",
            "number": "4"
          }
        },
        "from": "2019-02-08T15:00:00Z",
        "datatype": "forecast"
      },
      {
        "location": {
          "longitude": "12.5683",
          "precipitation": {
            "minvalue": "0.0",
            "unit": "mm",
            "maxvalue": "0.0",
            "value": "0.0"
          },
          "symbol": {
            "id": "Cloud",
            "number": "4"
          },
          "latitude": "55.6761",
          "altitude": "12"
        },
        "to": "2019-02-08T16:00:00Z",
        "datatype": "forecast",
        "from": "2019-02-08T14:00:00Z"
      },
      {
        "location": {
          "longitude": "12.5683",
          "latitude": "55.6761",
          "altitude": "12"
        },
        "to": "2019-02-08T16:00:00Z",
        "datatype": "forecast",
        "from": "2019-02-08T13:00:00Z"
      },
      ...
    ]
  }
}
-}
