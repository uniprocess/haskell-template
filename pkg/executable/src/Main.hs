--------------------------------------------------------------------------------
--
-- Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
--
--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import           Control.Concurrent
  ( setNumCapabilities
  )
import           Data.Maybe
  ( fromMaybe
  , listToMaybe
  )
import           GHC.Conc
  ( getNumProcessors
  )
import           System.Environment
  ( getArgs
  , getExecutablePath
  )

import           Uniprocess.Effects.Granulated.Instances
  ()
import           Uniprocess.Effects.Restricted.Instances
  ()
import qualified Uniprocess.Effects.Restricted.Instances as IIO
import qualified Uniprocess.Network.HTTPS                as HTTPS
import qualified Uniprocess.Network.TLS                  as TLS
import qualified Uniprocess.Process                      as UNI
import qualified Uniprocess.Toolbox.Web.Server           as WEB

--------------------------------------------------------------------------------

optimal
  :: IO ()

tlsPort
  :: IO Word

exePath
  :: IO String

isolated
  :: TLS.Context a
  => a
  -> IO ()

main
  :: IO ()

--------------------------------------------------------------------------------

optimal =
  getNumProcessors >>= setNumCapabilities

tlsPort =
  getArgs >>= pure . fromMaybe 8443 . listToMaybe . (map read)

exePath =
  getExecutablePath >>= \ p -> pure $ take (length p - n) p
  where
    n = length "uniprocess"

isolated =
  IIO.rio . UNI.process . HTTPS.Server

--------------------------------------------------------------------------------

main =
  optimal >>= \ ____ ->
  tlsPort >>= \ port ->
  exePath >>= \ path ->
  WEB.server port path isolated
