--------------------------------------------------------------------------------
--
-- Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Uniprocess.Data.SHS.SHA.SHA1
  ( bytes
  , digest
  ) where

--------------------------------------------------------------------------------

import           Data.Bits
    ( Bits
    , bitSizeMaybe
    , complement
    , shiftL
    , shiftR
    , xor
    , (.&.)
    , (.|.)
    )
import           Data.Maybe
    ( fromMaybe
    )
import           Data.Word
    ( Word32
    , Word8
    )

--------------------------------------------------------------------------------

type ByteString = [ Word8 ]

--------------------------------------------------------------------------------

bytes
  :: ByteString
  -> ByteString

digest
  :: ByteString
  -> ByteString

--------------------------------------------------------------------------------

bytes =
  tail . toHex . compute

digest =
  tail . toByte . compute

--------------------------------------------------------------------------------

(.-.) :: Bits a => a -> a
(.<.) :: Bits a => a -> Int -> a
(.>.) :: Bits a => a -> Int -> a
(.+.) :: Bits a => a -> a   -> a

rotl
  :: (Bits a)
  => Int
  -> a
  -> a

{- Outcommented cos of -Wall -Werror flags

rotr
  :: (Bits a)
  => Int
  -> a
  -> a

shr
  :: (Bits a)
  => Int
  -> a
  -> a

-}

choice
  :: (Bits a)
  => a
  -> a
  -> a
  -> a

parity
  :: (Bits a)
  => a
  -> a
  -> a
  -> a

majority
  :: (Bits a)
  => a
  -> a
  -> a
  -> a

f
  :: (Bits a)
  => Int
  -> a
  -> a
  -> a
  -> a

k
  :: Int
  -> Word32

pad
  :: Integer
  -> ByteString

parse
  :: ByteString
  -> [ ByteString ]

h
  :: [ Word32 ]

compute
  :: ByteString
  -> Integer

--------------------------------------------------------------------------------

{-

* [§1] Figure 1: Secure Hash Algorithm Properties

+-------------+--------------+------------+-----------+---------------------+
| Algorithm   | Message Size | Block Size | Word Size | Message Digest Size |
|             |    (bits)    |   (bits)   |   (bits)  |       (bits)        |
+-------------+--------------+------------+-----------+---------------------+
| SHA-1       | < 2^064      | 0512       | 32        | 160                 |
| SHA-224     | < 2^064      | 0512       | 32        | 224                 |
| SHA-256     | < 2^064      | 0512       | 32        | 256                 |
| SHA-384     | < 2^128      | 1024       | 64        | 384                 |
| SHA-512     | < 2^128      | 1024       | 64        | 512                 |
| SHA-512/224 | < 2^128      | 1024       | 64        | 224                 |
| SHA-512/256 | < 2^128      | 1024       | 64        | 256                 |
+-------------+--------------+------------+-----------+---------------------+

-}

--------------------------------------------------------------------------------

-- [§2.2.2] Symbols and Operations

(.-.) x   = complement x
(.<.) x y = x `shiftL` y
(.>.) x y = x `shiftR` y
(.+.) x y = x `xor`    y

rotl n x =
  (x .<. n) .|. (x .>. (w - n))
  where
    w = fromMaybe n $ bitSizeMaybe x

{- Outcommented cos of -Wall -Werror flags

rotr n x =
  (x .>. n) .|. (x .<. (w - n))
  where
    w = fromMaybe n $ bitSizeMaybe x

shr n x =
  x .>. n

-}

--------------------------------------------------------------------------------

-- [§4.1] Functions

choice x y z =
  (x .&. y) .+. ((.-.) x .&. z)

parity x y z =
  x .+. y .+. z

-- [§4.1.1] SHA-1 Functions

majority x y z =
  (x .&. y) .+. (x .&. z) .+. (y .&. z)

f t x y z
  | t < 20    = choice   x y z
  | t < 40    = parity   x y z
  | t < 60    = majority x y z
  | t < 80    = parity   x y z
  | otherwise = error $ "Shouldn't be possible (f): " ++ show t

--------------------------------------------------------------------------------

-- [§4.2] Constants

k t
  -- constants [§4.2.1]
  | t < 20    = 0x5A827999
  | t < 40    = 0x6ED9EBA1
  | t < 60    = 0x8F1BBCDC
  | t < 80    = 0xCA62C1D6
  | otherwise = error $ "Shouldn't be possible (k): " ++ show t

--------------------------------------------------------------------------------

-- [§5.1] Padding the Message

-- [§5.1.1] SHA-1, SHA-224 and SHA-256

pad l =
  -- length m + 1 bit + k zeros ≡ 448 mod 512
  --
  -- m = [ 'a', 'b', 'c' ]
  -- l = 3 x 8 (bits) = 24
  --
  -- 448 - (24 + 1) = 423
  --                                423        64
  --                              ------- -----------
  -- 01100001 01100010 01100011 1 00...00 00...011000
  -- -------- -------- --------                ------
  --   'a'      'b'      'c'                     24
  --
  -- (8 * 3) + 1 + 423 + 64 = 512
  --
  -- Note: 1 bit is represented as 128 (dec) which is 1000 0000 (bin)
  128 : aux (((448 - (len + 1)) .&. 511) .>. 3)
  where
    len   = l * 8
    aux 0 =
      -- length as 64 bytes (8 * Word8 of 8 bytes each)
      (fromIntegral $ len        .>. 56) :
      (fromIntegral $ len .<. 08 .>. 56) :
      (fromIntegral $ len .<. 16 .>. 56) :
      (fromIntegral $ len .<. 24 .>. 56) :
      (fromIntegral $ len .<. 32 .>. 56) :
      (fromIntegral $ len .<. 40 .>. 56) :
      (fromIntegral $ len .<. 48 .>. 56) :
      (fromIntegral $ len .<. 56 .>. 56) :
      []
    aux i = 000 : aux (i - 1)

--------------------------------------------------------------------------------

-- [§5.2] Parsing the Message

-- [§5.2.1] SHA-1, SHA-224 and SHA-256

parse =
  -- 512 bits = 64 bytes
  chunksOf 64

--------------------------------------------------------------------------------

-- [§5.3] Setting the Initial Hash Value (H(0))

-- [§5.3.1] SHA-1

h =
  [ 0x67452301
  , 0xEFCDAB89
  , 0x98BADCFE
  , 0x10325476
  , 0xC3D2E1F0
  ]

--------------------------------------------------------------------------------

-- [§6] SECURE HASH ALGORITHMS

-- [§6.1] SHA-1

-- [§6.1.2] SHA-1 Hash Computation

compute =
  aux 0 False h . parse
  where
    aux _ True  hv [    ] =
      000000000000000001 .<. 161  .|. -- toHex doesn't pad 0s
      ((fromIntegral h0) .<. 128) .|.
      ((fromIntegral h1) .<. 096) .|.
      ((fromIntegral h2) .<. 064) .|.
      ((fromIntegral h3) .<. 032) .|.
      ((fromIntegral h4)        )
      where
        (h0:h1:h2:h3:h4:_) = hv
    aux l False hv [    ] =
      aux l True hv $ parse $ pad 0
    aux l False hv (x:[]) =
      aux len True hv $ parse $ x ++ pad len
      where
        len = l + (fromIntegral $ length x)
    aux l ipd hv (x:xs) =
      len `seq` hv' `seq` aux len ipd hv' xs
      where
        len = l + 64
        sch = schedule x
        hv' = sch `seq` rounds hv sch

-- 1. Prepare the message schedule, {W t}:
schedule :: ByteString -> Schedule
schedule =
  msg 16 . sch . chunksOf 4
  where
    sch =
      aux 0 empty
      where
        aux _ acc [    ] = acc
        aux t acc (x:xs) = aux (t + 1) (ins (w32 x) t acc) xs
    w32 =
      aux 24 0
      where
        aux _ acc [    ] = acc
        aux i acc (b:bs) = aux (i - 8) (x .<. i .|. acc) bs
          where
            x = fromIntegral b
    msg =
      aux
      where
        aux t acc
          | 80 > t    =
            aux (t + 1) $
            ins
            (
              rotl 1 $
              idx (t - 03) acc .+.
              idx (t - 08) acc .+.
              idx (t - 14) acc .+.
              idx (t - 16) acc
            ) t acc
          | otherwise = acc

-- 3. For t=0 to 79:
rounds :: [ Word32 ] -> Schedule -> [ Word32 ]
rounds hv@(h0:h1:h2:h3:h4:_) ws =
  aux 0 hv
  where
    aux t (a:b:c:d:e:_)
      | 80 > t    =
        let
          t' = rotl 05 a + f t b c d + e + (k t) + (idx t ws)
          e' = d
          d' = c
          c' = rotl 30 b
          b' = a
          a' = t'
        in
          aux (t + 1)
          $ a' `seq` b' `seq` c' `seq` d' `seq` e' `seq`
          [ a',      b',      c',      d',      e'     ]
      | otherwise =
        let
          h0' = h0 + a
          h1' = h1 + b
          h2' = h2 + c
          h3' = h3 + d
          h4' = h4 + e
        in
            h0' `seq` h1' `seq` h2' `seq` h3' `seq` h4' `seq`
          [ h0',      h1',      h2',      h3',      h4'     ]
    aux _ _____________ =
      error $ "Shouldn't be possible (rounds -> aux)"
rounds _____________________ __ =
  error $ "Shouldn't be possible (rounds)"

--------------------------------------------------------------------------------

-- HELPERS

chunksOf :: Int -> ByteString -> [ ByteString ]
chunksOf _ [] = [               ]
chunksOf n bs = x : chunksOf n xs
  where
    (x,xs) = splitAt n bs

toBase :: Int -> (Integer -> Integer) -> Integer -> ByteString
toBase base fn =
  aux []
  where
    aux acc 0 = acc
    aux acc n =
      aux (r : acc) c
      where
        c = n .>. base
        r = toEnum . fromIntegral $ fn (n - c * 1 .<. base)
toByte :: Integer -> ByteString
toByte =
  toBase 8 {- 2^8 = 256 -} id
toHex' :: Bool -> Integer -> ByteString
toHex' cap =
  toBase 4 {- 2^4 = 016 -} aux
  where
    aux x
      | x < 0x0A  = 48 + x
      | x < 0x10  = cc + x
      | otherwise = error "Shouldn't be possible to reach (toHex')"
    cc = if cap then 55 else 87
toHex :: Integer -> ByteString
toHex =
  toHex' False

-- F U N C T I O N A L  P E A R L S
-- Red-Black Trees in a Functional Setting
--
-- CHRIS OKASAKI
-- School of Computer Science, Carnegie Mellon University
-- 5000 Forbes Avenue, Pittsburgh, Pennsylvania, USA 15213
-- (e-mail: cokasaki@cs.cmu.edu)

-- Red-Black Trees
data Color  = R | B
data Tree a = E | T Color (Tree a) a (Tree a)

-- Simple Set Operations
type Set a = Tree a

empty :: Set a
empty = E

{- Outcommented cos of -Wall -Werror flags

member :: Ord a => a -> Set a -> Bool
member x  E = False
member x (T _ a y b)
  | x <  y = member x a
  | x == y = True
  | x >  y = member x b

-}

-- Insertions
insert :: Ord a => a -> Set a -> Set a
insert e s =
  blk $ aux s
  where
    blk (T _ a y b) = T B a y b
    blk ___________ = error $ "Shouldn't be possible (insert -> blk)"
    aux  E = T R E e E
    aux (T c a y b)
      | e <  y = bal c (aux a) y      b
      | e == y = T   c      a  y      b
      | e >  y = bal c      a  y (aux b)
    aux ___________ = error $ "Shouldn't be possible (insert -> aux)"
    bal B (T R (T R a x b) y c) z d = T R (T B a x b) y (T B c z d)
    bal B (T R a x (T R b y c)) z d = T R (T B a x b) y (T B c z d)
    bal B a x (T R (T R b y c) z d) = T R (T B a x b) y (T B c z d)
    bal B a x (T R b y (T R c z d)) = T R (T B a x b) y (T B c z d)
    bal c a x b                     = T c a x b

type Schedule = Set (Int, Word32)

-- O(log n) insertions
ins :: Word32 -> Int -> Schedule -> Schedule
ins x i s =
  insert (i,x) s

-- O(log n) lookups
idx :: Int -> Schedule -> Word32
idx i =
  aux
  where
    aux (T _ a y b)
      | i <  fst y = aux a
      | i == fst y = snd y
      | i >  fst y = aux b
    aux ___________ = error $ "Shouldn't be possible (idx -> aux)"

instance Show Color where
  show R = "Red"
  show B = "Black"

instance Show a => Show (Tree a) where
  show = aux 0
    where
      aux l  E            =
        replicate l ' ' ++ "nil"
      aux l (T c lt x rt) =
        replicate l ' ' ++ show c ++ ": " ++ show x ++ "\n" ++
        replicate n ' ' ++ aux  n lt                ++ "\n" ++
        replicate n ' ' ++ aux  n rt
        where
          n = l + 1
