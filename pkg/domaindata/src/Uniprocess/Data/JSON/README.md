Text.JSON (text) -> Uniprocess.Data.JSON
========================================

Since we need an easy-to-use and `Safe` serializer from and to `JSON`, we sadly
discovered that both exclude [Data.Aeson][aeson] as well as [Text.JSON][json] as
they rely on [Data.Text][text], which is a module that can't be added to a
`Safe` package (can't bed trusted with the `-trust=text` flag).

Therefore we found out that it wasn't that difficult to remove all `text`
dependencies from [Text.JSON][json] so we could provide the end-users with the
possibility to easily write code to transform their data from and to `JSON`:

```haskell
data Foo = Foo
  { foo :: Int
  , bar :: String }
  deriving (Data, Typeable)

fooToJSON
  :: Data a => a -> L8.ByteString
fooToJSON =
  L8.pack . encodeJSON

fooFromJSON
  :: Data a => L8.ByteString -> a
fooFromJSON =
  decodeJSON . L8.unpack
```

Since we have removed the `text` dependency, we don't think the name
[Data.Text][text] give sense anymore. Therefore we have chosen the following
name to avoid any conflicts with other packages, prefixing with the projects
name: `Uniprocess.Data.JSON`, which is a single interface wrapping and exposing
the module `Text.JSON.Generic` which itself exposes `Text.JSON`.

All code from [Text.JSON][json] is clearly outcommented with `start` and `end`
tags and is placed in a `Internal` subfolder.

A few remarks, as we have removed support for `text`, the following sub-modules
can't be used anymore:

* `Text.JSON.Parsec`

* `Text.JSON.Pretty`

* `Text.JSON.ReadP`

Also, the import of [Data.Generics][datagenerics], is not neccesary anymore as
it can be replaced with `Data.Data` which is already part of `base`. There was
still need for it's submodule `Data.Generics.Aliases` though.

> **Note**: Since I have hooked up `stylish-haskell` to my `before-save-hook` in
> `emacs`, with my `keep my OCD happy` settings, the code format my differ from
> the initial.

[aeson]        : https://github.com/bos/aeson
[json]         : https://github.com/GaloisInc/json
[text]         : https://github.com/haskell/text
[datagenerics] : https://github.com/dreixel/syb
