--------------------------------------------------------------------------------
--
-- Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Uniprocess.Data.Byte
  ( toBase
  , toBit
  , toByte
  , toHex
  , toHexLow
  , unpack
  ) where

--------------------------------------------------------------------------------

import           Data.Bits
    ( Bits
    , shiftL
    , shiftR
    )
import           Data.Word
    ( Word8
    )

--------------------------------------------------------------------------------

type ByteString = [ Word8 ]

--------------------------------------------------------------------------------

(.<.)
  :: Bits a
  => a
  -> Int
  -> a
(.>.)
  :: Bits a
  => a
  -> Int
  -> a

unpack
  :: ByteString
  -> [ Char ]

toBase
  :: (Bits a, Integral a)
  => Int
  -> (a -> a)
  -> a
  -> ByteString
toBit
  :: (Bits a, Integral a)
  => a
  -> ByteString
toHex'
  :: (Bits a, Integral a)
  => Bool
  -> a
  -> ByteString
toHex
  :: (Bits a, Integral a)
  => a
  -> ByteString
toHexLow
  :: (Bits a, Integral a)
  => a
  -> ByteString
toByte
  :: (Bits a, Integral a)
  => a
  -> ByteString

--------------------------------------------------------------------------------

(.<.) x y = x `shiftL` y
(.>.) x y = x `shiftR` y

unpack =
  map (toEnum . fromIntegral)

toBase base f =
  aux []
  where
    aux acc 0 = acc
    aux acc n =
      aux (r : acc) c
      where
        c =                   n         .>. base
        r = fromIntegral $ f (n - c * 1 .<. base)

toBit =
  toBase 1 {- 2^1 = 002 -} (+ 48)

toHex' cap =
  toBase 4 {- 2^4 = 016 -} aux
  where
    aux x
      | x < 0x0A  = 48 + x
      | x < 0x10  = cc + x
      | otherwise = error "Shouldn't be possible to reach (toHex')"
    cc = if cap then 55 else 87

toHex =
  toHex' True

toHexLow =
  toHex' False

toByte =
  toBase 8 {- 2^8 = 256 -} id
