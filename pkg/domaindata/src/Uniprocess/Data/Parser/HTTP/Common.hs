--------------------------------------------------------------------------------
--
-- Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE OverloadedStrings #-}

--------------------------------------------------------------------------------

module Uniprocess.Data.Parser.HTTP.Common
  ( findSubByteString
  )
where

--------------------------------------------------------------------------------

import qualified Data.ByteString.Lazy.Char8 as L8

--------------------------------------------------------------------------------

findSubByteString
  :: L8.ByteString
  -> L8.ByteString
  -> Maybe Int

--------------------------------------------------------------------------------

findSubByteString ss bs =
  if aux < 0 then Nothing else Just $ fromIntegral $ aux
  where
    aux       = if n < m then -1 else sub m m m
    sub c i 0 =
      if com i 0 then c - m else -1
    sub c i j =
      if c > n   then            -1
      else
        if com i j then sub c (i-1) (j-1) else sub c' c' m
      where
        c' = c + 1
    -- O(n/c) length returns the length of a ByteString as an Int64.
    n = L8.length bs - 1
    m = L8.length ss - 1
    -- O(1) ByteString index (subscript) operator, starting from 0.
    com i j = L8.index bs i == L8.index ss j
