--------------------------------------------------------------------------------
--
-- Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
--
--------------------------------------------------------------------------------

module Uniprocess.Network.TLS
  ( Context
    ( pull
    , push
    )
  ) where

--------------------------------------------------------------------------------

import           Control.Monad.IO.Class
    ( MonadIO
    )
import qualified Data.ByteString.Lazy.Char8 as L8

--------------------------------------------------------------------------------

class Context a where
  pull
    :: MonadIO m
    => a
    -> m L8.ByteString
  push
    :: MonadIO m
    => a
    -> L8.ByteString
    -> m ()
