--------------------------------------------------------------------------------
--
-- Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
--
--------------------------------------------------------------------------------

module Uniprocess.Network.HTTPS
  ( Web
    ( Server
    , Socket
    , context
    )
  ) where

--------------------------------------------------------------------------------

data Web a
  = Server { context :: a }
  | Socket { context :: a }
