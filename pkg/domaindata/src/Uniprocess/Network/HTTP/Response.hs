--------------------------------------------------------------------------------
--
-- Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
--
--------------------------------------------------------------------------------

module Uniprocess.Network.HTTP.Response
  ( Response (..)
  , Status (..)
  )
where

--------------------------------------------------------------------------------

import qualified Data.ByteString.Lazy.Char8 as L8

--------------------------------------------------------------------------------

{- HyperText Transfer Protocol (Response message):

The response message consists of the following:

* A status line which includes the status code and reason message (e.g.,
  HTTP/1.1 200 OK, which indicates that the client's request succeeded).

* Response header fields (e.g., Content-Type: text/html).

* An empty line.

* An optional message body.

-}

data Status
  = S101 -- Switching Protocols (Web Sockets)
  | S200
  | S400
  | S403
  | S404
  | S405
  | NotSupported Int
  deriving Show

data Response
  = Response
    { status  :: Status
    , headers :: [ (String, String) ]
    , body    :: Maybe L8.ByteString
    }
  deriving Show

{-

Connection: close -- Always use for each req/res WebServer (not Socket)
-- https://tools.ietf.org/html/rfc2616#section-14.10

Accept: application/json
Accept: text/html
Accept: text/plain

Accept-Charset: utf-8

Allow: GET, POST -- To be used for a 405 Method not allowed

Content-Length: 348

Content-Type: application/json; charset=utf-8
Content-Type: text/html; charset=utf-8
Content-Type: text/plain; charset=utf-8

Upgrade: websocket

-}
