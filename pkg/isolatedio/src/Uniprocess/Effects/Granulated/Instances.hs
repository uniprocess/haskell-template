--------------------------------------------------------------------------------
--
-- Public domain: CC0, creativecommons.org/share-your-work/public-domain/cc0
--
--------------------------------------------------------------------------------

{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE UndecidableInstances #-}

--------------------------------------------------------------------------------

module Uniprocess.Effects.Granulated.Instances where

--------------------------------------------------------------------------------

import qualified Uniprocess.Effects.Granulated           as Granulated
import qualified Uniprocess.Effects.Restricted           as Restricted
import           Uniprocess.Effects.Restricted.Instances
  ()
import qualified Uniprocess.Effects.Restricted.Instances as IsolatedIO

--------------------------------------------------------------------------------

instance Granulated.CmdM IsolatedIO.RIO where
  out =
    Restricted.out
  outBS =
    Restricted.outBS
  outLBS =
    Restricted.outLBS

--------------------------------------------------------------------------------

instance Granulated.DateTimeM IsolatedIO.RIO where
  currentDate =
    Restricted.currentDate
  currentTime =
    Restricted.currentTime
  iso8601 =
    Restricted.iso8601

--------------------------------------------------------------------------------

instance Granulated.RandM IsolatedIO.RIO where
  bytes =
    Restricted.bytes

--------------------------------------------------------------------------------

instance Granulated.WebClientM IsolatedIO.RIO where
  get  =
    Restricted.get
  get' =
    Restricted.get'
  post  =
    Restricted.post
  post' =
    Restricted.post'

--------------------------------------------------------------------------------

instance Granulated.WebServerM IsolatedIO.RIO where
  recv =
    Restricted.recv
  send =
    Restricted.send
