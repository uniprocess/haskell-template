--------------------------------------------------------------------------------
--
-- Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
--
-------------------------------------------------------------------------------

{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE UndecidableInstances #-}

--------------------------------------------------------------------------------

module Uniprocess.Effects.Restricted.Instances
  ( RIO
    ( rio
    )
  ) where

--------------------------------------------------------------------------------

import qualified Data.ByteString.Char8         as C8
import qualified Data.ByteString.Lazy.Char8    as L8
import           Data.Time.Calendar
  ( toGregorian
  )
import           Data.Time.Clock
  ( getCurrentTime
  , utctDay
  )
import           Data.Time.Format
  ( defaultTimeLocale
  , formatTime
  )
import qualified Network.TLS                   as TLS
import qualified System.Random                 as RND

import qualified Uniprocess.Effects.Granulated as Granulated
import qualified Uniprocess.Effects.Restricted as Restricted
import qualified Uniprocess.Network.HTTPS      as HTTPS
import qualified Uniprocess.Network.TLS        as UTLS
import qualified Uniprocess.Toolbox.Web.Client as WebClient

--------------------------------------------------------------------------------

newtype RIO a = RestrictedIO { rio :: IO a }

instance Functor RIO where
  fmap f m = RestrictedIO $      f <$> rio m

instance Applicative RIO where
  pure      = RestrictedIO . pure
  (<*>) f m = RestrictedIO $ rio f <*> rio m

instance Monad RIO where
  return    = RestrictedIO . return
  (>>=) m f = RestrictedIO $ rio m >>= rio . f

--------------------------------------------------------------------------------

instance Granulated.CmdM RIO => Restricted.CmdM RIO where
  out =
    RestrictedIO .    putStrLn
  outBS =
    RestrictedIO . C8.putStrLn
  outLBS =
    RestrictedIO . L8.putStrLn

--------------------------------------------------------------------------------

instance Granulated.DateTimeM RIO => Restricted.DateTimeM RIO where
  currentDate =
    RestrictedIO $ toGregorian . utctDay <$> getCurrentTime
  currentTime =
    RestrictedIO $ getCurrentTime
  iso8601 =
    RestrictedIO $ (formatTime defaultTimeLocale fm) <$> getCurrentTime
    where
      fm = "%FT%T%0QZ"

--------------------------------------------------------------------------------

instance Granulated.RandM RIO => Restricted.RandM RIO where
  bytes n =
    RestrictedIO $ take n . RND.randoms <$> RND.newStdGen

--------------------------------------------------------------------------------

instance Granulated.WebClientM RIO => Restricted.WebClientM RIO where
  get  host port path =
    RestrictedIO $ WebClient.get  host port path
  get' host port path =
    RestrictedIO $ WebClient.get' host port path False
  post  host port path body =
    RestrictedIO $ WebClient.post  host port path body
  post' host port path body =
    RestrictedIO $ WebClient.post' host port path body False

--------------------------------------------------------------------------------

instance UTLS.Context TLS.Context where
  pull ctx =
    L8.fromChunks . (:[]) <$> TLS.recvData ctx
  push =
    TLS.sendData

instance Granulated.WebServerM RIO => Restricted.WebServerM RIO where
  recv web =
    RestrictedIO $ UTLS.pull $ HTTPS.context web
  send web bs =
    RestrictedIO $ UTLS.push  (HTTPS.context web) bs
