--------------------------------------------------------------------------------
--
-- Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
--
-------------------------------------------------------------------------------

module Uniprocess.Effects.Restricted
  ( CmdM
    ( out
    , outBS
    , outLBS
    )
  , DateTimeM
    ( currentDate
    , currentTime
    , iso8601
    )
  , RandM
    ( bytes
    )
  , WebClientM
    ( get
    , get'
    , post
    , post'
    )
  , WebServerM
    ( recv
    , send
    )
  ) where

--------------------------------------------------------------------------------

import qualified Data.ByteString               as BS
import qualified Data.ByteString.Lazy.Char8    as L8
import           Data.Time
  ( UTCTime
  )
import           Data.Word
  ( Word8
  )

import qualified Uniprocess.Effects.Granulated as Granulated
import qualified Uniprocess.Network.HTTPS      as HTTPS
import qualified Uniprocess.Network.TLS        as TLS

--------------------------------------------------------------------------------

class Granulated.CmdM m => CmdM m where
  out
    :: String
    -> m ()
  outBS
    :: BS.ByteString
    -> m ()
  outLBS
    :: L8.ByteString
    -> m ()

--------------------------------------------------------------------------------

class Granulated.DateTimeM m => DateTimeM m where
  currentDate
    :: m (Integer,Int,Int)
  currentTime
    :: m UTCTime
  iso8601
    :: m (String)

--------------------------------------------------------------------------------

class Granulated.RandM m => RandM m where
  bytes
    :: Int
    -> m [ Word8 ]

--------------------------------------------------------------------------------

class Granulated.WebClientM m => WebClientM m where
  get
    :: String
    -> Maybe Int
    -> Maybe String
    -> m (Either String L8.ByteString)
  get'
    :: String
    -> Maybe Int
    -> Maybe String
    -> m (Either String L8.ByteString)
  post
    :: String
    -> Maybe Int
    -> Maybe String
    -> String
    -> m (Either String L8.ByteString)
  post'
    :: String
    -> Maybe Int
    -> Maybe String
    -> String
    -> m (Either String L8.ByteString)

--------------------------------------------------------------------------------

class Granulated.WebServerM m => WebServerM m where
  recv
    :: TLS.Context   a
    => HTTPS.Web     a
    -> m L8.ByteString
  send
    :: TLS.Context a
    => HTTPS.Web   a
    -> L8.ByteString
    -> m ()
