--------------------------------------------------------------------------------
--
-- Public domain: CC0, creativecommons.org/share-your-work/public-domain/cc0
--
--------------------------------------------------------------------------------

module Uniprocess.Effects.Granulated
  ( CmdM
    ( out
    , outBS
    , outLBS
    )
  , DateTimeM
    ( currentDate
    , currentTime
    , iso8601
    )
  , RandM
    ( bytes
    )
  , WebClientM
    ( get
    , get'
    , post
    , post'
    )
  , WebServerM
    ( recv
    , send
    )
  ) where

--------------------------------------------------------------------------------

import qualified Data.ByteString            as BS
import qualified Data.ByteString.Lazy.Char8 as L8
import           Data.Time
  ( UTCTime
  )
import           Data.Word
  ( Word8
  )

import qualified Uniprocess.Network.HTTPS   as HTTPS
import qualified Uniprocess.Network.TLS     as TLS

--------------------------------------------------------------------------------

class (Monad m) => CmdM m where
  out
    :: String
    -> m ()
  outBS
    :: BS.ByteString
    -> m ()
  outLBS
    :: L8.ByteString
    -> m ()

--------------------------------------------------------------------------------

class Monad m => DateTimeM m where
  currentDate
    :: m (Integer,Int,Int)
  currentTime
    :: m UTCTime
  iso8601
    :: m (String)

--------------------------------------------------------------------------------

class Monad m => RandM m where
  bytes
    :: Int
    -> m [ Word8 ]

--------------------------------------------------------------------------------

class Monad m => WebClientM m where
  get
    :: String
    -> Maybe Int
    -> Maybe String
    -> m (Either String L8.ByteString)
  get'
    :: String
    -> Maybe Int
    -> Maybe String
    -> m (Either String L8.ByteString)
  post
    :: String
    -> Maybe Int
    -> Maybe String
    -> String
    -> m (Either String L8.ByteString)
  post'
    :: String
    -> Maybe Int
    -> Maybe String
    -> String
    -> m (Either String L8.ByteString)

--------------------------------------------------------------------------------

class Monad m => WebServerM m where
  recv
    :: TLS.Context   a
    => HTTPS.Web     a
    -> m L8.ByteString
  send
    :: TLS.Context a
    => HTTPS.Web   a
    -> L8.ByteString
    -> m ()
