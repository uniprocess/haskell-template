--------------------------------------------------------------------------------
--
-- Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE OverloadedStrings #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import           Test.Hspec
    ( Spec
    , describe
    , hspec
    , it
    , shouldBe
    )
import           Test.QuickCheck
    ( property
    )

import           Data.ByteString.Lazy   as LBS
import           Data.Word
    ( Word8
    )

import qualified Uniprocess.Data.Base64 as B64
import qualified Uniprocess.Data.JSON   as JSON

--------------------------------------------------------------------------------

propTestsBase64
  :: [ Spec ]

propCaseBase64
  :: Spec

unitTestsBase64
  :: [ Spec ]

testCaseBase64
  :: Spec

--------------------------------------------------------------------------------

propTestsJSON
  :: [ Spec ]

propCaseJSON
  :: Spec

--------------------------------------------------------------------------------

main
  :: IO ()

--------------------------------------------------------------------------------

b64dec =
  "Man is distinguished, not only by his reason, but by this singular \
  \passion from other animals, which is a lust of the mind, that by a \
  \perseverance of delight in the continued and indefatigable generation \
  \of knowledge, exceeds the short vehemence of any carnal pleasure."

b64enc =
  "TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1\
  \dCBieSB0aGlzIHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3\
  \aGljaCBpcyBhIGx1c3Qgb2YgdGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFu\
  \Y2Ugb2YgZGVsaWdodCBpbiB0aGUgY29udGludWVkIGFuZCBpbmRlZmF0aWdhYmxl\
  \IGdlbmVyYXRpb24gb2Yga25vd2xlZGdlLCBleGNlZWRzIHRoZSBzaG9ydCB2ZWhl\
  \bWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4="

propTestsBase64 =
  [ it ("(decode . encode) bytes == Just bytes") $
    property decenc
  ]
  where
    decenc :: [ Word8 ] -> Bool
    decenc =
      \ bs -> (B64.decode . B64.encode) bs == Just bs

propCaseBase64 =
  do
    describe "Propety-base testing Base64" $
      do
        mapM_ id propTestsBase64

unitTestsBase64 =
  [ it ("Base64 encode") $
    (B64.encode . LBS.unpack) b64dec `shouldBe` LBS.unpack b64enc
  , it ("Base64 decode") $
    (B64.decode . LBS.unpack) b64enc `shouldBe` (Just $ LBS.unpack b64dec)
  , it ("Base64 decode + encode") $
    (B64.decode . B64.encode . LBS.unpack) b64dec
    `shouldBe`
    (Just $ LBS.unpack b64dec)
  ]

testCaseBase64 =
  do
    describe "Unit testing Base64" $
      do
        mapM_ id unitTestsBase64

--------------------------------------------------------------------------------

propTestsJSON =
  [ it "(JSON.decodeJSON . JSON.encodeJSON) str == str" $
    property twice
  ]
  where
    twice :: String -> Bool
    twice str = (JSON.decodeJSON . JSON.encodeJSON) str == str

propCaseJSON =
  do
    describe "Propety-base testing JSON " $
      do
        mapM_ id propTestsJSON

--------------------------------------------------------------------------------

main = hspec $
  do
    propCaseBase64
    testCaseBase64
    propCaseJSON
