#!/usr/bin/env bash

################################################################################
##
## Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
##
################################################################################

clear

find . -name "effects.svg" -delete -print
find . -name       "*.pdf" -delete -print
