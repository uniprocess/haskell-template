#!/usr/bin/env Rscript

################################################################################
##
## Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
##
################################################################################

# install.packages("ggplot2")
# install.packages("rPython")
# install.packages("svglite")

# install.packages("http://genome.crg.es/~didac/ggsunburst/ggsunburst_0.0.10.tar.gz", repos=NULL, type="source")

library(ggsunburst)

sb <- sunburst_data("effects.csv", type = "lineage", sep=",", node_attributes = "Effects")
gp <- sunburst(sb, leaf_labels = F, node_labels = F, rects.fill.aes = "Effects") + scale_fill_grey()
ggsave(file="effects.svg", plot=gp, height = 3, width = 7)

## Reference
# - http://genome.crg.es/~didac/ggsunburst/dsv_format.html
