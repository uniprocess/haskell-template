#!/usr/bin/env bash

################################################################################
##
## Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
##
################################################################################

clear

docker build --network=host -t haskell-base-builder --file ./der/Dockerfile.builder .
docker build --network=host -t uniprocess           --file ./der/Dockerfile         .
