#!/usr/bin/env bash

################################################################################
##
## Uniprocess, (c) 2018 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
##
################################################################################

clear

for i in $(seq -f "%05g" 1 64); do
    echo "Spawned client ID:" $i
    curl --insecure --verbose \
	 --header "Content-Type: application/json" \
	 --data '{"city":"Copenhagen,Denmark"}' \
	 https://localhost.uniprocess.org:8443/ >& /dev/null &
done
